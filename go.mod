module gitlab.com/jhinrichsen/adventofcode2022

go 1.18

require (
	github.com/otiai10/gosseract/v2 v2.4.0
	gonum.org/v1/gonum v0.12.0
)

require golang.org/x/exp v0.0.0-20191002040644-a1355ae1e2c3 // indirect
